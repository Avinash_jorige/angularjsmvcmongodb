﻿app.factory("GetInitData", ["$http", "$q", "$state", "$window", "$rootScope", function ($http, $q, $state, $window, $rootScope) {
    delete $http.defaults.headers.common['X-Requested-With'];
    var fac = {};

    fac.GetAllData = function () {
        return $http.get("/Default/getData");
    }

    fac.AddSample = function (item) {
        return $http.post("/Default/AddSample", item).success(function (response) {
            alert(response.message);
        });
    }

    return fac;
}])
