﻿app.controller("DefaultController", function (GetInitData, $scope, $state) {
    this.title = "Angular + ASP.NET + MongoDB";
    GetInitData.GetAllData().then(function (d) {

        if (d.data['status'] == 400) {
            alert("Error : " + d.data.message);
        }

        if (d.data['status'] == 200) {
            $scope.data = d.data.data;
        }        
    });

    $scope.AddSample = function () {
        if ($scope.sampleData != null) {

            var sampleInfo = {};
            var address = {};
            address.streetAddress = $scope.sampleData.street;
            address.city = $scope.sampleData.city;
            address.state = $scope.sampleData.state;
            address.postalCode = $scope.sampleData.postalCode;

            var phoneNumber = [];
            var phoneType = {};
            phoneType.type = 'Home';
            phoneType.number = $scope.sampleData.home;
            phoneNumber.push(phoneType);
            phoneType = {};
            phoneType.type = 'Fax';
            phoneType.number = $scope.sampleData.fax;
            phoneNumber.push(phoneType);

            sampleInfo.firstName = $scope.sampleData.firstName;
            sampleInfo.lastName = $scope.sampleData.lastName;
            sampleInfo.age = $scope.sampleData.age;
            sampleInfo.address = address;
            sampleInfo.phoneNumber = phoneNumber;

            GetInitData.AddSample(sampleInfo).then(function (d) {
                if (d.data['status'] == 400) {
                    alert("Error : " + d.data.message);
                }

                if (d.data['status'] == 200) {
                    $scope.data = d.data.data;
                    $scope.sampleDataInformation = {};
                    $state.go('getAll');
                }
            });
        }
        else {
            alert("Something went wrong... Fill the form again after page reload.");
        }
    }

});